data DNA = A|T|C|G deriving (Show,Eq,Read)

dna :: DNA -> DNA
dna A = T
dna T = A
dna C = G
dna G = C

f :: [DNA] -> [DNA]
f x = map dna x

main = do
   x <- getLine
   let m = map (\k -> read [k]::DNA) x
   putStrLn $ (concat $ map show (f m)) ++ " "