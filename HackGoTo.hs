#!/usr/bin/env stack
-- stack -- install-ghc --resolver lts-5.13 runghc --package http-conduit
{-# LANGUAGE OverloadedStrings #-}
import qualified Data.ByteString.Lazy.Char8 as L8
import Network.HTTP.Simple
import Control.Monad
hack :: Control.Monad.Catch.MonadThrow m => [Char] -> m Request
main :: IO ()
main = do
    x <- getLine
    let y = "https://goto.msk.ru/vault/api/get_balance/" ++ x
    k <- parseRequest k
    response <- httpLBS (k)
L8.putStrLn $ getResponseBody response